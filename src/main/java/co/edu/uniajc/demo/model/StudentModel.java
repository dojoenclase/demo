package co.edu.uniajc.demo.model;

import javax.persistence.*;

@Entity
@Table(name="student")
public class StudentModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "stu_id")
    private long id;

    @Column(name = "stu_nombre")
    private String name;

    @Column(name = "stu_apellido")
    private String lastname;

    @Column(name = "stu_edad")
    private Integer age;

    @Column(name = "stu_estado")
    private Boolean state;

    public StudentModel(){
        //constructor vacio se utilza el data transform
        // jaxon para poder manipular los formatos json
    }

    public StudentModel(long id, String name, String lastname, Integer age, Boolean state){
        this.id = id;
        this.name = name;
        this.lastname = lastname;
        this.age = age;
        this.state = state;

    }

    public long getId() { return id; }

    public void setId(long id) { this.id = id; }

    public String getName() { return name;  }

    public void setName(String name) { this.name = name; }

    public String getLastname() { return lastname;  }

    public void setLastname(String lastname) { this.lastname = lastname; }

    public Integer getAge() { return age;  }

    public void setAge(Integer age) { this.age = age; }

    public Boolean getState() { return state; }

    public void setState(Boolean state) { this.state = state; }

}
