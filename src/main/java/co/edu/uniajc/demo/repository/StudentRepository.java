package co.edu.uniajc.demo.repository;

import co.edu.uniajc.demo.model.StudentModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentRepository extends JpaRepository<StudentModel, Long> {
    //con esto tenemos el CRUD heredado

       List<StudentModel> findAllByNameContains(String name);
       StudentModel getById(long id);

       //metodos
       @Query(nativeQuery = true, value = "SELECT" +
               "stu_id, stu_nombre, " +
               "stu_apellido, stu_edad " +
               "stu_estado" +
               "FROM student"+
               "WHERE stu_edad=: age")
                List<StudentModel> findAge (@Param(value = "age") Integer age);
}
