create table "student" (

                stu_id serial not null,
                stu_nombre character varying(50) not null,
                stu_apellido character varying(50) not null,
                stu_edad integer not null,
                stu_estado boolean not null,
                primary key (stu_id)
                );

alter table "student" owner to postgres;